#include <chrono>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <string_view>
#include <csignal>

#include <rx.hpp>

#include <zmq.hpp>

#include <glog/logging.h>

#include "zmqpub.hpp"
#include "iri.hpp"

namespace iota {
namespace utils {
	
static volatile sig_atomic_t terminated = 0;
	
void handle_stop_signals(int signum) 
{	
	LOG(INFO) << "Received signale: " << signum;
	//Another implementation i thought about was
	//to call from here to a new method (which is yet to be written)
	//called "zmqPublisher::terminate()" 
	//zmqPublisher::terminate() will send a message to 
	//an "inproc" zmq::socket_t which the publisher will have 
	//to first bind to and later poll, and if a message is received on that socket
	//then publisher exits, that would enable other threads holding zmqPublisher
	//object with the abillity to nicely terminate the polling.
	terminated = 1;
}

void zmqPublisher(rxcpp::subscriber<std::shared_ptr<iri::IRIMessage>> s,
                  const std::string& uri) {
	
  signal(SIGKILL, handle_stop_signals);
  signal(SIGINT, handle_stop_signals);
	
  std::array<char, 2048> buf;

  zmq::context_t context(1);
  zmq::socket_t subscriber(context, ZMQ_SUB);
  subscriber.setsockopt(ZMQ_IDENTITY, "iota/utils/common", 5);
  subscriber.setsockopt(ZMQ_SUBSCRIBE, "", 0);
  subscriber.connect(uri);

  auto poller = std::make_unique<zmq::poller_t>();

  auto handler = std::function<void(void)>();
  poller->add(subscriber, ZMQ_POLLIN, handler);

  while (!terminated) {
    buf.fill('\0');

    poller->wait(std::chrono::milliseconds(-1));

    int size = zmq_recv(subscriber, buf.data(), buf.size() - 1, 0);
    if (size == -1) continue;

    std::string_view view(buf.data(),
                          static_cast<std::string_view::size_type>(size));

    auto msg = iri::payloadToMsg(view);

    if (msg) {

		   s.on_next(std::move(msg));
	  }
    }

  s.on_completed();
}

}  // namespace utils
}  // namespace iota
